﻿# Fµll-Life: The RPG (Source SDK 2013)

## Introduction
Based off of *HλLF-LIFE 2*, ***FµLL-LIFE*** is designed to add more RPG-style
elements to the main mechanics of the former.

*Fµll-Life* uses Source SDK 2013.

*"What is a free man to do*  
*when the lights finally turn on?"*


## Mechanics/Modifications

### Weapon Rebalancing
Even at its hardest difficulty, *Half-Life 2* is not a difficult game, epsecially for those
seasoned with first-person shooters. To make the gameplay more challenging, changes have been 
implemented.

Firearm and explosive weapon damage has been increased substantially, forcing the player to 
utilise map structures for cover. Additionally, melee weapons and attacks have been noticably
weakened. The combination of these two changes forces players to rely more on firearms and 
explosives to deal damage to enemies, a task made harder by the limiting of firearms ammunition
carry capacities. For example, while *Half-Life 2* players can carry a maximum of 150 rounds of 
9mm ammunition on their character, *Full-Life* players can only carry 50.

### Magical Objects/Abilities
In order to partially compensate for the increased difficulty and to make the game more interesting,
the mod aims to add certain magical weapons and abilities. Currently, this section is highly indev.

